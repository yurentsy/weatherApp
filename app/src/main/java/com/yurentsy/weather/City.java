package com.yurentsy.weather;

import java.util.List;

/**
 * Created by Brony on 09.03.2018.
 */

public class City {

    public static List<City> cities;

    private String name;
    private String temperature;
    private String time;
    private String state;

    public City(String name, String temperature) {
        this.name = name;
        this.temperature = temperature;
        this.time = "0:00 PM";
        this.state = "Sunny";
    }

    public String getName() {
        return name;
    }


    public String getTemperature() {
        return temperature;
    }

    public String getTime() {
        return time;
    }

    public String getState() {
        return state;
    }
}
