package com.yurentsy.weather;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CitiesListActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cities_list);

        //get RecyclerView
        recyclerView = findViewById(R.id.cities_recycler_view);

        //set data
        loadSharedPreference();

        //begin set RecyclerView
        //set LayaoutManager
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        //set Adapter
        adapter = new myAdapter(City.cities);
        recyclerView.setAdapter(adapter);
        //end set RecyclerView
    }

    public void showPopup(View view) {
        PopupMenu popup = new PopupMenu(this, view);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_add:
                        addElement();
                        break;
                    case R.id.menu_clear:
                        clearList();
                        break;
                    default:
                        return false;
                }
                adapter.notifyDataSetChanged();
                return true;
            }
        });
        popup.inflate(R.menu.main_menu);
        popup.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveSharedPreference();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add:
                addElement();
                break;
            case R.id.menu_clear:
                clearList();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        adapter.notifyDataSetChanged();
        return true;
    }

    private void addElement() {
        City.cities.add(new City("city", "0"));
    }

    private void clearList() {
        City.cities.removeAll(City.cities);
    }

    private void saveSharedPreference() {
        sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        //здесь сохранение списка выбранных городов

        editor.apply();
    }

    private void loadSharedPreference() {
        sharedPreferences = getPreferences(MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();

        //здесь загрузка списка городов

        City.cities = new ArrayList<>(Arrays.asList(
                new City("Moscow", "-10"),
                new City("Saint Petersburg", "-14"),
                new City("Murmansk", "-20"),
                new City("Yekaterinburg", "-18")
        ));
    }

    private void showCityInfoActivity(Integer cityId) {
        Intent intent = CityInfoActivity.newIntent(this, cityId);
        startActivity(intent);
    }

    private class myAdapter extends RecyclerView.Adapter<MyViewHolder> {

        private List<City> cities;

        public myAdapter(List<City> cities) {
            this.cities = cities;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
            return new MyViewHolder(inflater.inflate(R.layout.activity_city_item, parent, false));
//            return new MyViewHolder(inflater, parent);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.bind(cities.get(position).getName(),
                    cities.get(position).getTime(),
                    cities.get(position).getTemperature());
        }

        @Override
        public int getItemCount() {
            return cities.size();
        }
    }


    private class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener {

        private final MenuItem.OnMenuItemClickListener onEditMenu = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_delete:
                        City.cities.remove(getAdapterPosition());
                        break;
                    default:
                        return false;
                }
                adapter.notifyDataSetChanged();
                return true;
            }
        };
        private TextView time;
        private TextView name;
        private TextView temperature;

        //not work
//        MyViewHolder(LayoutInflater inflater, ViewGroup parent) {
//            super(inflater.inflate(R.layout.activity_city_item, parent, false));
//            itemView.setOnClickListener(this);
//
//            this.time = itemView.findViewById(R.id.city_time);
//            this.name = itemView.findViewById(R.id.city);
//            this.temperature = itemView.findViewById(R.id.city_temperature);
//
//            View view = inflater.inflate(R.layout.activity_city_item, parent, false);
//            view.setOnCreateContextMenuListener(this);
//        }

        MyViewHolder(View v) {
            super(v);
            itemView.setOnClickListener(this);

            this.time = itemView.findViewById(R.id.city_time);
            this.name = itemView.findViewById(R.id.city);
            this.temperature = itemView.findViewById(R.id.city_temperature);

            v.setOnCreateContextMenuListener(this);
        }

        void bind(String name, String time, String temperature) {
            if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                this.time.setText(time);
                this.name.setText(name);
                this.temperature.setText(temperature);
            }
        }

        @Override
        public void onClick(View view) {
            showCityInfoActivity(getAdapterPosition());
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.context_menu, contextMenu);
            contextMenu.findItem(R.id.menu_delete).setOnMenuItemClickListener(onEditMenu);
        }
    }
}
