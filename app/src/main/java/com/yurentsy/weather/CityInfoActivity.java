package com.yurentsy.weather;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

public class CityInfoActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String INTENT_CITY_ID = "city_id";

    private TextView name;
    private TextView state;
    private TextView temperature;

    public static Intent newIntent(Context context, Integer city) {
        Intent intent = new Intent(context, CityInfoActivity.class);
        intent.putExtra(INTENT_CITY_ID, city);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_info_drawer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        int id = getIntent().getIntExtra(INTENT_CITY_ID, 0);

        CityInfoFragment cityFragment = new CityInfoFragment();
        cityFragment.setCityId(id);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.city_info_fragment, cityFragment)
                .commit();

//        setContentView(R.layout.activity_city_info);
//
//        name = findViewById(R.id.city);
//        state = findViewById(R.id.state);
//        temperature = findViewById(R.id.temperature);
//
//        Bundle bundle = getIntent().getExtras();
//        if (bundle != null) {
//            name.setText(City.cities.get(bundle.getInt(INTENT_CITY_ID)).getName());
//            state.setText(City.cities.get(bundle.getInt(INTENT_CITY_ID)).getState());
//            temperature.setText(City.cities.get(bundle.getInt(INTENT_CITY_ID)).getTemperature());
//        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_share) {
        } else if (id == R.id.nav_send) {
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
