package com.yurentsy.weather;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Brony on 13.03.2018.
 */

public class CityInfoFragment extends Fragment {

    private int cityId;

    public void setCityId(int id) {
        this.cityId = id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_city_info, container, false);

        if (savedInstanceState != null) {
            cityId = savedInstanceState.getInt(CityInfoActivity.INTENT_CITY_ID);
        }

        TextView name = view.findViewById(R.id.city);
//        TextView state = view.findViewById(R.id.state);
        TextView temperature = view.findViewById(R.id.temperature);

        FragmentManager chiFragmentManager = getChildFragmentManager();
        WeatherInfo weatherInfo = new WeatherInfo();

        Bundle bundle = getActivity().getIntent().getExtras();
        if (bundle != null) {
            name.setText(City.cities.get(bundle.getInt(CityInfoActivity.INTENT_CITY_ID)).getName());
//            state.setText(City.cities.get(bundle.getInt(CityInfoActivity.INTENT_CITY_ID)).getState());
            weatherInfo.setState(City.cities.get(bundle.getInt(CityInfoActivity.INTENT_CITY_ID)).getState());
            temperature.setText(City.cities.get(bundle.getInt(CityInfoActivity.INTENT_CITY_ID)).getTemperature());
        }

        weatherInfo.setArguments(bundle);

        chiFragmentManager
                .beginTransaction()
                .replace(R.id.weather_info, weatherInfo)
                .commit();

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(CityInfoActivity.INTENT_CITY_ID, cityId);
    }
}
