package com.yurentsy.weather;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class WeatherInfo extends Fragment {

    private String stateValue;

    public void setState(String state) {
        this.stateValue = state;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.weather_info_fragment, container, false);
        TextView state = view.findViewById(R.id.state);

        Bundle bundle = getArguments();

        state.setText(stateValue);
        return view;
    }
}
